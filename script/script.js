const addButton = document.querySelector(".button--add");
const section2 = document.querySelector(".section--2");
let addInput = document.querySelector(".add--input");
const modifyListe = document.querySelector("modify");
const removeListe = document.querySelector("remove");
const section3 = document.querySelector(".section--3");
let arrToDo = [];

addButton.addEventListener("click", function (e) {
  e.preventDefault();
  let task = {
    name: addInput.value,
    createdAt: new Date(),
    complet: "pending",
  };
  arrToDo.push(task);
  displayList(arrToDo);
});

const removeFromList = function (name) {
  console.log(name);
  arrToDo = arrToDo.filter((task) => task.name != name);
  console.log(arrToDo);
  displayList(arrToDo);
};

const modifFromList = function (name) {
  console.log(name);
  section3.style.display = "flex";
  section2.style.display = "none";
};

function displayList(arrToDo) {
  section2.textContent = "";
  arrToDo.forEach(function (todo, i) {
    const html = `
    <article class="article--secondSection">
      <p class="task-toDo">${todo.name}</p>
      <div class="icons">
        <button class="icon--border modify">
          <img onClick=modifFromList("${todo.name}") src="icons/modif.svg" class="img--icon" />
        </button>
        <button class="icon--border remove">
          <img onClick=removeFromList("${todo.name}") src="icons/trash.svg" class="img--icon" />
        </button>
        </div>
    </article>`;

    section2.insertAdjacentHTML("afterbegin", html);
    addInput.value = "";
  });
}

const updateButton = document.querySelector(".update-button");
const cancelButton = document.querySelector(".cancel-button");

cancelButton.addEventListener("click", function (e) {
  e.preventDefault();
  console.log("cancel");
  section3.style.display = "none";
  section2.style.display = "flex";
});

updateButton.addEventListener("clik", function (e) {
  e.preventDefault();
  const updateName = document.querySelector("add--input-update");
  const pending = document.querySelector("add--pending-update");
  let task = {
    name: updateName.value,
    complet: pending.value,
    updateAt: new Date(),
  };
  arrToDo.push(task);
  displayList(arrToDo);
});
